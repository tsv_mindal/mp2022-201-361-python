
import turtle

turtle.shape("turtle")

length = 10
for i in range(20):
    turtle.forward(length)
    turtle.left(90)
    length += 10

turtle.exitonclick()