import turtle

turtle.shape("turtle")

a = 40
b = 10
for i in range(b):
    
    if i%2 == 0:
        turtle.forward(a)
        turtle.left(-90)
        turtle.forward(b)
        turtle.left(-90)
    else:
        turtle.forward(a)
        turtle.left(90)
        turtle.forward(b)
        turtle.left(90)


turtle.exitonclick()