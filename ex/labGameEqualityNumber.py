import random
from random import randint
from colorama import Fore

n = 6
q = 3
array = []

def choice_level(op):
    match op:
        case 0:            
            choice_task(q, 16, 0, 0)       
        case 1:
            choice_task(q, 64, 0, 0)
        case 2:
            choice_task(q, 256, 0, 0)
        case _:
            print(Fore.RED + 'Уровень не выбран') 


def generate_num(a):
    for i in range (a):
        array.append(i)

def decimalToBinary(n):
    return bin(n).replace("0b","")

def choice_task(q, lev, right, wrong):
    global i
    for i in range(q):
        generate_num(lev)
        num = random.choice(array)
        rand_num = randint(0,99)
        if rand_num % 2 == 0:
            print(num, ' =? ', decimalToBinary(num))
            ans = input('Введите ответ: ')
            if ans.casefold() == 'y':
                right = right + 1
            else:
                wrong = wrong + 1     
        else:
            print(num, ' =?hex ', f'{int(decimalToBinary(num), 2):X}')
            ans = input('Введите ответ: ')
            if ans.casefold() == 'y':
                right = right + 1
            else:
                wrong = wrong + 1 
    print (Fore.GREEN + 'Количество правильных ответов = ', right)
    print (Fore.RED + 'Количество неправильных ответов = ', wrong)


print('Выберите уровень игры: \n0-лёгкий \n1-средний \n2-сложный')
op = int(input())
choice_level(op)
print(Fore.MAGENTA + "ИГРА ЗАВЕРШЕНА.\nХотите повторить попытку?")
start_game = input(Fore.WHITE + 'Если да, введите Y ')
if start_game.casefold() == 'Y':
    choice_level(op)
else:
    print(Fore.MAGENTA + 'Спасибо за попытку!')