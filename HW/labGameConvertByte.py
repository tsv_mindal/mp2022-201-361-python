import random
from random import randint
from colorama import Fore

n = 6
q = 3
array = []

def choice_level(op):
    match op:
        case 0:            
            choice_task(q, 8, 0, 0)       
        case 1:
            choice_task(q, 16, 0, 0)
        case 2:
            choice_task(q, 32, 0, 0)
        case 3:
            choice_task(q, 64, 0, 0)
        case _:
            print(Fore.RED + 'Уровень не выбран') 

def generate_bin(a):
    for i in range(a):
        b = bin(i)[2:]
        l = len(b)
        b = str(0) * (n - l) + b
        array.append(b)

def choice_task(q, lev, right, wrong):
    global i
    for i in range(q):
        generate_bin(lev)
        num = random.choice(array)
        rand_num = randint(0,99)
        if rand_num % 2 == 0:
            print(num + ' =? def')
            ans = input('Введите ответ: ')
            if int(ans) == int(num, base = 2):
                right = right + 1
            else:
                wrong = wrong + 1     
        else:
            print(num + ' =? hex')
            ans = input('Введите ответ: ')
            if ans == f'{int(num, 2):X}':
                right = right + 1
            else:
                wrong = wrong + 1 
    print (Fore.GREEN + 'Количество правильных ответов = ', right)
    print (Fore.RED + 'Количество неправильных ответов = ', wrong)
        
print('Выберите уровень игры: \n0-детский \n1-лёгкий \n2-средний \n3-сложный')
op = int(input())
choice_level(op)
print(Fore.MAGENTA + "ИГРА ЗАВЕРШЕНА.\nХотите повторить попытку?")
start_game = input(Fore.WHITE + 'Если да, введите Y ')
if start_game == 'Y'.casefold():
    choice_level(op)
else:
    print(Fore.MAGENTA + 'Спасибо за попытку!')