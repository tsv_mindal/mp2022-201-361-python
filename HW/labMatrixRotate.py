import random
import numpy as np

print('Введите количество строк матрицы: ')
rm = int(input())
print('Введите количество столбцов матрицыx: ')
cm = int(input())
array = []
for i in range(rm):
    array.append([])
    for j in range(cm):
        array[i].append(random.randint(10,100))

print('Матрица: ')
print(*array, sep='\n')

print('Выберите операцию над матрицей: \n0-повернуть по часовой стрелке на 90° \n1-повернуть против часовой стрелки на 90° \n2-отразить по вертикали \n3-отразить по горизонтали')
op = int(input())
match op:
    case 0:
        arr_0 = np.rot90(array, k=-1)
        print(arr_0)
    case 1:
        arr_1 = np.rot90(array)
        print(arr_1)
    case 2:
        arr_2 = np.fliplr(array)
        print(arr_2)
    case 3:
        arr_3 = np.flipud(array)
        print(arr_3)
    case _:
        print('Операция не выбрана')