
#Вывести в консоль все доступные цвета и фоны

def colors_16(color_):
    return("\033[2;{num}m {num} \033[0;0m".format(num=str(color_)))

print("Все доступные цвета:")
print(' '.join([colors_16(x) for x in range(30, 38)]))

print("Все доступные фоны:")
print(' '.join([colors_16(x) for x in range(40, 48)]))


