import turtle 
from random import randint, randrange
import time
import math

turtle.shape("turtle")

a = turtle.numinput('Длина поля', 'Введите число', 100, minval = 10, maxval = 1000)
b = turtle.numinput('Ширина поля', 'Введите число', 100, minval = 10, maxval = 1000)

x = -600
y = 350


def rectangle(x,y,width,height):
    turtle.pen(fillcolor="green", pencolor = "brown", pensize=40)
    turtle.up()
    turtle.goto(x,y)
    turtle.down()
    turtle.begin_fill()
    turtle.fd(width)
    turtle.right(90)
    turtle.fd(height)
    turtle.right(90)
    turtle.fd(width)
    turtle.right(90)
    turtle.fd(height)
    turtle.end_fill()
    turtle.right(90)
    
rectangle(x,y,a,b)

block_size = (a-50)/10
row_block = (b-50-block_size) /block_size
row_block = math.floor(row_block)


def block_1(x,y,a):
    turtle.up()
    kol_block = randint(0,a)
    for i in range(kol_block):
        color_rand = randint(0,4)
        if color_rand != 3 :
            turtle.fillcolor("black")
        else:
            turtle.fillcolor("white")
        x_1 = x+25+a*100-50
        pos_block = randrange(x+25, x_1, block_size )
        turtle.goto(pos_block, y-25)
        turtle.begin_fill()
        turtle.fd(block_size)
        turtle.right(90)
        turtle.fd(block_size)
        turtle.right(90)
        turtle.fd(block_size)
        turtle.right(90)
        turtle.fd(block_size)
        turtle.end_fill()
        turtle.right(90)


def block_2(x,y,b):
    turtle.goto(x+25,y-25)
    time.sleep(3)
    for i in range (row_block):
        block_1(x,y,a/100)
        y = y - block_size

block_2(x,y,b)

turtle.exitonclick()