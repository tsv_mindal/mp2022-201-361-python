
from tabulate import tabulate

a = 8
b = 8
chessboard = []
col_1 = ['♖','♘','♗','♕','♔','♗','♘','♖']
col_2 = []
x = 0
for i in range(a):
    col_2.append('♙')

field_1= []
field_1.append("№")
for i in range(65,73):
    field_1.append(chr(i))

for i in range(a):
    chessboard.append([])
    for j in range(a):
        if (j==0):
            chessboard[i].append(b)
            b = b-1
        if (i==0) or (i==7):
            chessboard[i].append(col_1[j])    
        if (i==1) or (i==6):
            chessboard[i].append(col_2[j])       

      
print(tabulate(chessboard, headers = field_1, tablefmt='grid', stralign='center'))


